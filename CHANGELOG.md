CHANGELOG
=========

1.1.1 (2013-12-18)
------------------

* removed orchestral/testbench from require-dev added missing required Illuminate packages
* passed container to User Provider and used that rather than facades
* tidied up test a little

1.1.0 (2013-12-13)
------------------

* updated framework requirement to 4.1.*

1.0.2 (2013-12-13)
------------------

* changed from using Facades within service provdider to accessing $this->app directly

1.0.1 (2013-10-08)
------------------

* fixed bug in passing session to Guard

1.0.0 (2013-09-05)
------------------

* initial release
